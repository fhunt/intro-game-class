﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	private Transform tf;
	public float rotateSpeed;
	public GameObject bulletPrefab;
	public Transform shotLocation;
	public float speed;
	private Vector3 startLocation;
	private bool isPaused;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		startLocation = tf.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.instance.isPaused) {
			return;
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			tf.position = startLocation;
		}
		//ROTATION CODE
		if (Input.GetKey (KeyCode.A)) {
			tf.Rotate (0, 0, rotateSpeed);
		}
		if(Input.GetKey(KeyCode.D)){
			tf.Rotate(0,0,-rotateSpeed);
		}
		//SHOOTER CODE
		//if (Input.GetKeyDown (KeyCode.Space)) {
			//Instantiate (bulletPrefab, shotLocation.position, shotLocation.rotation);
		//}
		//MOVEMENT CODE
		if (Input.GetKey (KeyCode.LeftShift)) {
			if (Input.GetKeyDown (KeyCode.LeftArrow)) {
				tf.position -= tf.right;
			}
			if (Input.GetKeyDown (KeyCode.RightArrow)) {
				tf.position += tf.right;
			}
			if (Input.GetKeyDown (KeyCode.UpArrow)) {
				tf.position += tf.up;
			}
			if (Input.GetKeyDown (KeyCode.DownArrow)) {
				tf.position -= tf.up;
			}
		}
		else {
			if (Input.GetKey (KeyCode.LeftArrow)) {
				tf.position -= tf.right * Time.deltaTime * speed;
				}
			if (Input.GetKey (KeyCode.RightArrow)) {
				tf.position += tf.right * Time.deltaTime * speed;
				}
			if (Input.GetKey (KeyCode.UpArrow)) {
				tf.position += tf.up * Time.deltaTime * speed;
				}
			if (Input.GetKey (KeyCode.DownArrow)) {
				tf.position -= tf.up * Time.deltaTime * speed;
				}
			}

	}
}
