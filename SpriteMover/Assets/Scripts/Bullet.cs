﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	private Transform tf;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		Destroy (this.gameObject, 1);
	}
	
	// Update is called once per frame
	void Update () {
		tf.position += tf.up;
	}
}
